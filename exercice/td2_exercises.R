#########################################
#
# Exercice : R graphics
#
# Auteur : Antoine Lamer, Mathilde Fruchart
#
#########################################

## Chargement des packages
library(dplyr)
library(ggplot2)
library(stringr)

## Chemins

path_import = "/Users/aananousoukaina/Desktop/M1 DSS/R/r_td2/exercice"

# ------------------------------------------------------------------------------------------

list.files(path_import)


############################################################################################
# 1 Chargement des données et data management
############################################################################################

data_src = read.csv2(file.path(path_import, "data_220929.csv"))
data_src$sexe = factor(data_src$sexe)
data_src$asa = factor(data_src$asa)
levels(data_src$asa) = c("ASA1", "ASA2", "ASA3", "ASA4", "ASA5")

##data_src$mois_fr = ordered(data_src$mois_fr, levels=c("Jan", "Fév", "Mar", "Avr", "Mai", "Jun", "Jui", "Aou", "Sep", "Oct", "Nov", "Dec"))

## Fin du data management

data = data_src

############################################################################################
# Consignes 
############################################################################################

# Vous pouvez rajouter des opérations de data management $
# pour calculer des nouvelles variables
# Si des variables présentent des valeurs aberrantes, vous pouvez les filter
# Ex : retirer les poids > 200 kg
# Les graphiques doivent être lisibles et interprétables

# Q1 : Histogramme du poids

data %>%
  filter(poids < 200) %>%
  ggplot(aes(x=poids))+
  geom_histogram(aes(y=..density..))

# Q2 : Histogramme du poids avec un axe des absisses comprenant les valeurs entre 0 et 150

hist(data$poids, breaks = 150)

data %>%
  filter(poids < 150) %>%
  ggplot(aes(x=poids))+
  geom_histogram(aes(y=..density..))
  xlim = 150

# Q4 : Courbe de densité du poids en fonction du sexe
# supprimer au préalable les poids > 200

data %>%
  filter(poids < 200) %>%
  ggplot(aes(x=poids, color=sexe))+
  geom_density()

#Le poids moyen des femmes est de 60kg environ (pique à ce niveau là) tandis que celui des hommes est à 80 environ (pique à ce niveau là)

# Q5 : Q4 + thème avec un fond blanc

data %>%
  filter(poids < 200) %>%
  ggplot(aes(x=poids, color=sexe))+
  theme_classic()+
  geom_density()

# Q6 Boxplots représentant la durée d'anesthésie par classe ASA

data %>%
  ggplot(aes(x = asa , y = duree_anesthesie)) +
  geom_boxplot()+ 
  coord_cartesian(ylim = c(0, 2000))

#On comprend ici que plus l'état de santé est mauvais moins nous avons de valeurs extremes et la médiane est plus élevée. 
#On peut constater que la durée d'anesthesie mediane de l'ASA4 est la plus petite 
#et celle de l'ASA 5 est la plus importante. Ce qui parait logique.  
#La médiane des 3 autres cat se superpose

# Q7 Représentation du nombre d'intervention par classe d'âge :
# [0-17[, [18-34[, [35-54[, [55-74[, >74

data <- data %>%
  mutate(classe_age = cut(age, 
                          breaks = c (0,17,34,54,74,100),
                          labels = c("[0-17[", "[18-34[", "[35-54[","[55-74[", ">74"), 
                          na.rm = TRUE))

data %>%
  filter(!is.na(classe_age))%>%
  ggplot(aes (x = classe_age)) +
  geom_bar(stat ="count")+
  ylab("nb_intervention")

#nous remarquons que la population recevant le plus grand nombre d'intervention sont les 35-54 ans (autour de 2000)
#suivi de très près des 55-74 ans (autour de 1700). 
#la population plus jeune (moins de 17ans) ou plus vieille (plus de 74ans) ne subisse pas beaucoup d'intervention comparé au 35-74 ans

# Q8 : Q7 avec proposition de nouvelles couleurs

data %>%
  filter(!is.na(classe_age))%>%
  ggplot(aes (x = classe_age, fill = classe_age))+
  geom_bar(stat ="count")+
  ylab("nb_intervention")+ 
  scale_fill_manual(values = c("[0-17[" = "red", 
                               "[18-34[" = "blue", 
                               "[35-54[" = "black",
                               "[55-74[" = "brown", 
                               ">74" = "pink"))

# Q9 : Q8 avec titre du graphique "Nombre d'interventions par classe d'âge"
# Retirer les libellés sur axes des abscisses et des ordonnées 

data %>%
  filter(!is.na(classe_age))%>%
  ggplot(aes (x = classe_age, fill = classe_age))+
  geom_bar(stat ="count")+
  xlab("")+
  ylab("")+
  scale_fill_manual(values = c("[0-17[" = "red", 
                               "[18-34[" = "blue", 
                               "[35-54[" = "black",
                               "[55-74[" = "brown", 
                               ">74" = "pink"))+
  ggtitle("Nombre d'interventions par classe d'âge")

# Q10 Représentation de la mortalité par sexe

data <- data %>% 
  mutate(mortalite = case_when(deces_sejour_interv == 1 ~ "deces",
                               deces_sejour_interv == 0 ~ "vivant"))

data %>%
  filter(mortalite == "deces") %>%
  ggplot(aes(x=sexe, fill= sexe))+
  geom_bar()+
  ylab("deces")+
  ggtitle("Mortalite en fonction du sexe")

#on constate que dans la population des personnes décédés, les hommes ont un nombre de deces légèrement plus élevé que celui des femmes 

# Q11 Représentation de l'IMC en fonction de la classe ASA
# IMC = Poids / Taille (m) ²

data <- data %>%
  mutate(IMC = poids/(taille*10^-2)^2)
  
data %>%
  filter(IMC<75) %>%
  group_by(asa) %>%
  ggplot(aes(x = asa, y = IMC)) +
  geom_boxplot() 

#j'ai décidé le "inférieur à 75" car il n'y a qu'une seule personne ayant un IMC > 75 
#et il est dans la classe 2 donc c'est plus facilement interpretable comme ça. 
#nous retrouvons dans ce boxplot encore de nombreuses valeurs extremes voire aberrantes notamment pour les classes 1,2,3
#cependant nous pouvons remarquer qu'avec un IMC médian de 25 les patients auront un état de santé moyen
#avec un IMC bas, nous remarquons que le patient a soit dans la majorité des cas un état de santé normal soit un état grave.
#la classe 5 recoit une faible distribution

# Q12 Représentation de la mortalité en fonction d'un IMC > 30 kg/m²

data %>%
  filter(mortalite == "deces", IMC > 30) %>%
  ggplot(aes(x=IMC, color = mortalite))+
  theme_classic()+
  geom_density()

data %>%
  filter(mortalite == "deces", IMC > 30) %>%
  ggplot(aes(y= IMC, color = mortalite))+
  geom_boxplot()+

#pour moi le graphique n'est pas vraiment cohérent, un histogramme ou diag en barre n'est pas pertinent ici
#le boxplot aurait pu etre interessant 
#mais il donne selon moi le meme niveau d'incoherence que la courbe de densité
#je ne comprends pas ????


# Q13 Réprésenter la mortalité en fonction de IMC > 30 kg/m² et categorie_asa

data %>%
  filter(mortalite == "deces", IMC > 30) %>%
  ggplot(aes(x = mortalite, y = IMC, group = asa, fill = asa))+
  geom_bar(stat = "identity", width = 0.5, position = "dodge")+
  scale_fill_manual(values = c("ASA2" = "red", 
                               "ASA3" = "pink", 
                               "ASA4" = "purple"))+
  ggtitle("Mortalite en fonction de IMC > 30 kg/m^2 x categorie_asa")

data %>%
  filter(mortalite == "deces", IMC > 30) %>%
  ggplot(aes(x = asa , y= IMC) ) +
  geom_boxplot( aes(fill = mortalite))

#de même ici qu'à la question 12
